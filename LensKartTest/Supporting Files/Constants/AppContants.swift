//
//  AppContants.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import Foundation
import UIKit

//****************************   MARK: App Constants    ***********************

let BASE_API_URL = "http://image.tmdb.org/t/p/w92"
var kAppName = "LensKartTest"

//****************************   MARK: typealias    ***********************
typealias JSONDictionary = [String:Any]
typealias JSONArray = [JSONDictionary]
typealias responseCallBack = ((Bool, String?, NSError?) -> ())
