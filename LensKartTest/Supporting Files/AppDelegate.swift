//
//  AppDelegate.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    //MARK:- Variables
    var window: UIWindow?

    
    //MARK:- Methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        kAppName = Bundle.main.infoDictionary?[StringConstants.kCFBundleName] as? String ?? ""
        return true
    }


}

