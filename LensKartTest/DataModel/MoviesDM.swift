//
//  MoviesDM.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import Foundation

extension MoviesVM{
    
    func parseMovies(movies : [JSONDictionary]){
        for movie in movies{
            self.movies.append(Movies(dictionary: movie))
        }
    }
}


//MARK:- Models
struct Movies {
    var backdrop_path = ""
    var title = ""
    var overview = ""
    var vote_average = 0.0
    
    init(){}
    
    init(dictionary: JSONDictionary) {
        backdrop_path = dictionary["backdrop_path"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
        overview = dictionary["overview"] as? String ?? ""
        vote_average = dictionary["vote_average"] as? Double ?? 0.0
        
    }
}
