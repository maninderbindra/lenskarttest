//
//  HomeVC.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import UIKit
import SDWebImage

class HomeVC: BaseVC {

   
    //MARK:- IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    //MARK:- Variables
    
    
    //MARK:- VC Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getMovies()
    }
    
    
    //MARK:- IBActions
    
    
    //MARK:- Custom Methods

}

//MARK:- API Calls
extension HomeVC {
    
    func getMovies(){
        MoviesVM.shared.getMovies { (success, message, error) in
            if error == nil{
                if success{
                    self.collectionView.reloadData()
                }else{
                    self.showAlert(message: message)
                }
            }else{
                self.showAlert(message: error?.localizedDescription)
            }
        }
        
    }
    
}

//MARK:- Collection View Methods
extension HomeVC : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MoviesVM.shared.movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCell
        let object = MoviesVM.shared.movies[indexPath.item]
        cell.movieImage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.movieImage.sd_setImage(with: URL(string: BASE_API_URL.appending(object.backdrop_path)), placeholderImage: nil, options: [], context: nil)
        cell.ratingLabel.text = "VA: \(object.vote_average)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let VC = MyStoyboard.Main.description.instantiateViewController(withIdentifier: "MovieDetailVC") as! MovieDetailVC
        VC.movie = MoviesVM.shared.movies[indexPath.item]
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.bounds.width-30)/3, height: self.collectionView.bounds.height)
    }
    
}


//MARK:- Collecion View Cell
//MARK:-(We can create Xib but due to time shortage i'm keeping inside collectionView)
class ImageCell : UICollectionViewCell{
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    
}
