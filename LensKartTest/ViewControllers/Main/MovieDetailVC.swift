//
//  MovieDetailVC.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import UIKit
import SDWebImage

class MovieDetailVC: BaseVC {

    
    //MARK:- IBOutlets
    
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieDescription: UILabel!
    
    //MARK:- Variables
    var movie = Movies()
    
    //MARK:- VC Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
    }
    
    func setUI(){
        self.movieImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        self.movieImageView.sd_setImage(with: URL(string: BASE_API_URL.appending(movie.backdrop_path)), placeholderImage: nil, options: [], context: nil)
        self.movieTitle.text = movie.title
        self.movieDescription.text = movie.overview
    }
    
    
    //MARK:- IBActions
    
    @IBAction func backButton(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    //MARK:- Custom Methods

}
