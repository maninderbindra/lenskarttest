//
//  BaseVC.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import UIKit

class BaseVC: UIViewController {

    enum MyStoyboard {
        case Main
        var description : UIStoryboard {
            switch self {
                case .Main: return UIStoryboard(name: "Main", bundle: nil)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    //MARK: *************   Show Alert   ***************
    func showAlert(message: String?, title:String = kAppName, otherButtons:[String:((UIAlertAction)-> ())]? = nil, cancelTitle: String = StringConstants.kOk,onWindow :Bool = false ,cancelAction: ((UIAlertAction)-> ())? = nil) {
        let newTitle = title.capitalized
        let newMessage = message
        let alert = UIAlertController(title: newTitle, message: newMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: cancelTitle, style: .default, handler: cancelAction))
        
        if otherButtons != nil {
            for key in otherButtons!.keys {
                alert.addAction(UIAlertAction(title: key, style: .default, handler: otherButtons![key]))
            }
        }
        if onWindow{
            let window = (UIApplication.shared.delegate as! AppDelegate).window
            window?.rootViewController?.present(alert, animated: true, completion: nil)
        }else{
            present(alert, animated: true, completion: nil)
        }
        
    }

}
