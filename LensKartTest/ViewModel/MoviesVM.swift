//
//  MoviesVM.swift
//  LensKartTest
//
//  Created by Mac on 29/05/21.
//

import Foundation


class MoviesVM{
    
    private init(){}
    static let shared = MoviesVM()
    
    var movies = [Movies]()
    
    func getMovies(response: @escaping responseCallBack){
        if let path = Bundle.main.path(forResource: "movies", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [JSONDictionary] {
                    self.parseMovies(movies: jsonResult)
                    response(true, "Success", nil)
                }
            } catch(let error) {
                response(false, "Fail to get file", error as NSError)
            }
        }
    }
    
    
}


